module.exports = {
  semi: false,
  singleQuote: false,
  tabWidth: 2,
  useTabs: false,
  trailingComma: "es5",
  jsxSingleQuote: true,
  printWidth: 80,
};
