
<!-- ABOUT THE PROJECT -->
## About The Project


This is an application done as a test task. It's web and desktop app, created with React.js and Electron.js

Because there were problems with the provided API, and other open source APIs also were not very stable, I've created mock API inside this application, but don't worry, it works just as real REST API service, the only thing is that the data is random, so the titles, descriptions, actors and posters won't match and images can repeat :)


<!-- GETTING STARTED -->
## Getting Started

Before starting, make sure that you have installed node.js and yarn, preferably the latest versions, if not, install before moving forward.
### Installation


1. Clone the repo

2. Install NPM packages using yarn
   ```sh
   yarn
   ```
3. Make sure you don't have any local server running on port 8080, if you do, either kill it, or change the port in [vite.config.ts](./vite.config.ts) and [electron.cjs](./public/electron.cjs) files.

4. Because the API is mock, if we need, we can manually set the probability of getting error from request by creating a file, named .env.local, and copying the content of [.env.local.example](./.env.local.example) and setting the number in percents, if not we can skip this step.

5. Run the web application locally
   ```sh
   yarn dev
   ```
6. Now you have your web app running, if you want to run desktop application too, run this command
    ```sh
       yarn desktop
    ```

<!-- CONTACT -->
## Contact

Lev Yesayan - levyeesayandev@gmail.com

[![LinkedIn][linkedin-shield]][linkedin-url]


[linkedin-url]: https://www.linkedin.com/in/lev-yesayan-202a12246/
[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=for-the-badge&logo=linkedin&colorB=555
