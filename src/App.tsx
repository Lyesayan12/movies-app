import Router from "./Router";
import { Provider } from "react-redux";
import { store } from "./store";
import { ToasterProvider } from "./contexts/toaster/ToasterContext";
import Toaster from "./contexts/toaster/Toaster";
import "./App.css";

function App() {
  return (
    <>
      <Provider store={store}>
        <ToasterProvider>
          <Toaster />
          <Router />
        </ToasterProvider>
      </Provider>
    </>
  );
}

export default App;
