import { configureStore } from "@reduxjs/toolkit";
import moviesSlice from "./movies-slice/movies";

export const store = configureStore({
  reducer: {
    movies: moviesSlice,
  },
});

export type RootState = ReturnType<typeof store.getState>;
