import { Movie, PartialMovie } from "../../types/movie-types";

export interface IMovieSlice {
  singleSelectedMovie: Movie | Record<string, never>;
  homepageMovies: Array<PartialMovie>;
  searchResults: Array<PartialMovie>;
}
