import { RootState } from "../index";
import { Movie, PartialMovie } from "../../types/movie-types";

export const selectSingleMovie = (state: RootState): Movie =>
  state?.movies?.singleSelectedMovie;
export const selectHomepageMovies = (state: RootState): Array<PartialMovie> =>
  state?.movies?.homepageMovies;

export const selectSearchResults = (state: RootState): Array<PartialMovie> => {
  return state?.movies?.searchResults;
};
