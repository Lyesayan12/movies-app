import { createSlice } from "@reduxjs/toolkit";
import { IMovieSlice } from "./interface";

export const initialState: IMovieSlice = {
  singleSelectedMovie: {},
  homepageMovies: [],
  searchResults: [],
};

const moviesSlice = createSlice({
  name: "movies",
  initialState,
  reducers: {
    selectSingleMovie: (state, { payload }) => {
      state.singleSelectedMovie = payload;
    },
    setHomepageMovies: (state, { payload }) => {
      state.homepageMovies = payload;
    },
    setSearchResults: (state, { payload }) => {
      state.searchResults = payload;
    },
  },
});
export const movieActions = moviesSlice.actions;

export default moviesSlice.reducer;
