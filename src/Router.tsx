import { BrowserRouter, Route, Routes } from "react-router-dom";
import MovieDetails from "./pages/movie-details/MovieDetails";
import Home from "./pages/home/Home";
import SearchResults from "./pages/search-results/SearchResults";
import NotFound from "./pages/not-found/NotFound";
import MainLayout from "./components/layout/MainLayout";

const Router = () => {
  const routesList = [
    { path: "/", Component: Home, props: {} },
    {
      path: "/movie-details/:id",
      Component: MovieDetails,
      props: {},
    },
    {
      path: "/search-results",
      Component: SearchResults,
      props: {},
    },
  ];

  return (
    <div>
      <BrowserRouter>
        <Routes>
          {routesList.map(({ path, Component, props }) => (
            <Route
              key={path}
              path={path}
              element={
                <MainLayout>
                  <Component {...props} />
                </MainLayout>
              }
            />
          ))}
          <Route path="*" element={<NotFound />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
};

export default Router;
