import styles from "./styles.module.css";
import { Link } from "react-router-dom";

interface IMovieCardProps {
  title: string;
  poster: string;
  id: string;
}

const MovieCard = ({ title, poster, id }: IMovieCardProps) => {
  return (
    <Link to={`/movie-details/${id}`}>
      <div className={styles.cardContainer}>
        <img src={poster} alt={title} className={styles.cardImage} />
        <div className={styles.cardTitle}>
          <span className={styles.titleText}>{title}</span>
        </div>
      </div>
    </Link>
  );
};

export default MovieCard;
