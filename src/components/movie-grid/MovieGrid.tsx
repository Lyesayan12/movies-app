import styles from "./styles.module.css";
import MovieCardSkeleton from "../movie-card-skeleton/MovieCardSkeleton";
import MovieCard from "../movie-card/MovieCard";
import { PartialMovie } from "../../types/movie-types";

interface IMovieGrid {
  loading: boolean;
  movieList: Array<PartialMovie>;
}
const MovieGrid = ({ loading, movieList = [] }: IMovieGrid) => {
  return (
    <div className={styles.container}>
      {loading
        ? new Array(10)
            .fill(1)
            .map((_el, index) => <MovieCardSkeleton key={index} />)
        : movieList.map((movie) => (
            <MovieCard
              key={movie.id}
              title={movie.title}
              poster={movie.poster}
              id={movie.id}
            />
          ))}
    </div>
  );
};

export default MovieGrid;
