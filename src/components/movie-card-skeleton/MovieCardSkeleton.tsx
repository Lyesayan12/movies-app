import styles from "./styles.module.css";

const MovieCardSkeleton = () => {
  return (
    <div className={styles.skeletonStyle}>
      <div className={styles.animatedBackground}></div>
      <div className={styles.textBlock}>
        <div className={styles.textLine}></div>
        <div className={`${styles.textLine} ${styles.w80}`}></div>
      </div>
    </div>
  );
};

export default MovieCardSkeleton;
