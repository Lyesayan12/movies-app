import { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import headerLogo from "../../assets/Cinema.jpeg";
import styles from "./styles.module.css";

const Header = () => {
  const [searchQuery, setSearchQuery] = useState("");
  const navigate = useNavigate();
  const handleSearchChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSearchQuery(event.target.value);
  };

  const handleSearchSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    navigate(`/search-results?query=${searchQuery}`);
    setSearchQuery("");
  };

  return (
    <header className={styles.header}>
      <Link to="/">
        <img src={headerLogo} alt="Logo" className={styles.logo} />{" "}
      </Link>
      <form onSubmit={handleSearchSubmit} className={styles.searchForm}>
        <input
          type="text"
          placeholder="Search movies..."
          value={searchQuery}
          onChange={handleSearchChange}
          className={styles.searchInput}
        />
        <button type="submit" className={styles.searchButton}>
          Search
        </button>
      </form>
    </header>
  );
};

export default Header;
