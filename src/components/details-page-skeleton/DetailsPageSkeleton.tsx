import styles from "./styles.module.css";

const DetailsPageSkeleton = () => {
  return (
    <div className={styles.container}>
      <div className={styles.skeletonTitle}></div>
      <div className={styles.content}>
        <div className={styles.skeletonPoster}></div>
        <div className={styles.details}>
          <div className={styles.skeletonText}></div>
          <h3>Actors</h3>
          <ul>
            {Array.from({ length: 3 }).map((_, index) => (
              <li key={index} className={styles.skeletonText}></li>
            ))}
          </ul>
          <h3>Reviews</h3>
          <div>
            {Array.from({ length: 2 }).map((_, index) => (
              <div
                key={index}
                className={`${styles.skeletonText} ${styles.skeletonReview}`}
              ></div>
            ))}
          </div>
          <h3>Keywords</h3>
          <ul>
            {Array.from({ length: 3 }).map((_, index) => (
              <li key={index} className={styles.skeletonText}></li>
            ))}
          </ul>
        </div>
      </div>
    </div>
  );
};

export default DetailsPageSkeleton;
