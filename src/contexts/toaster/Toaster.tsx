import { useToaster } from "./ToasterContext";
import styles from "./styles.module.css";

const Toaster = () => {
  const { toasts, removeToast } = useToaster();

  return (
    <div className={styles.toaster}>
      {toasts.map((toast) => (
        <div
          key={toast.id}
          className={styles.toast}
          onClick={() => removeToast(toast.id)}
        >
          {toast.message}
        </div>
      ))}
    </div>
  );
};

export default Toaster;
