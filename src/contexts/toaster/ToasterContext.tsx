import {
  createContext,
  useContext,
  useState,
  ReactNode,
  FunctionComponent,
} from "react";

type Toast = {
  id: number;
  message: string;
  duration?: number;
};

type addToast = (message: string, duration?: number) => void;
type removeToast = (id: number) => void;

type ToasterContextType = {
  toasts: Toast[];
  addToast: addToast;
  removeToast: removeToast;
};

const ToasterContext = createContext<ToasterContextType | undefined>(undefined);

export const useToaster = () => {
  const context = useContext(ToasterContext);
  if (!context) {
    throw new Error("useToaster must be used within a ToasterProvider");
  }
  return context;
};

type ToasterProviderProps = {
  children: ReactNode;
};

export const ToasterProvider: FunctionComponent<ToasterProviderProps> = ({
  children,
}) => {
  const [toasts, setToasts] = useState<Toast[]>([]);

  const removeToast: removeToast = (id) => {
    setToasts((toasts) => toasts.filter((toast) => toast.id !== id));
  };
  const addToast: addToast = (message, duration) => {
    const id = new Date().getTime();
    setToasts((toasts) => [...toasts, { id, message, duration }]);
    setTimeout(() => removeToast(id), duration);
  };

  return (
    <ToasterContext.Provider value={{ toasts, addToast, removeToast }}>
      {children}
    </ToasterContext.Provider>
  );
};
