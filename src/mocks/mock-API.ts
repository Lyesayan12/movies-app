export function mockFetcher<T>(data): Promise<T> {
  return new Promise((resolve, reject) => {
    const probabilityOfError = import.meta.env.VITE_ERROR_PROBABILITY || 0;
    setTimeout(() => {
      if (Math.random() * 100 > probabilityOfError) {
        resolve(data);
      } else {
        reject({ message: "Something went wrong, please refresh the page" });
      }
    }, 800);
  });
}
