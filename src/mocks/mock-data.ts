export const mockMovies = [
  {
    id: "1",
    title: "Eternal Visions",
    description:
      "A journey through the realms of time with a mysterious guide.",
    poster:
      "https://image.tmdb.org/t/p/original/xlaY2zyzMfkhk0HSC5VUwzoZPU1.jpg",
    actors: ["Alex Ray", "Mia Stone"],
    reviews: [
      {
        reviewer: "John Doe",
        text: "An intriguing tale of mystery and time travel.",
      },
      {
        reviewer: "Jane Smith",
        text: "Visually stunning and beautifully narrated.",
      },
    ],
    keywords: ["time travel", "adventure", "mystery"],
  },
  {
    id: "2",
    title: "War of Tomorrow",
    description:
      "In a world ravaged by war, a group of survivors fight to rebuild civilization.",
    poster:
      "https://atthemovies.uk/cdn/shop/products/Gladiator2000us27x40in195u.jpg?v=1621385091",
    actors: ["John Carter", "Lily Evans"],
    reviews: [
      {
        reviewer: "Alice Johnson",
        text: "A post-apocalyptic thriller with a heart.",
      },
      { reviewer: "Bob Brown", text: "Gritty, dark, and hopeful." },
    ],
    keywords: ["post-apocalyptic", "war", "survival"],
  },
  {
    id: "3",
    title: "The Last Experiment",
    description:
      "A rogue scientist creates a life-changing device, but at what cost?",
    poster:
      "https://image.tmdb.org/t/p/original/xlaY2zyzMfkhk0HSC5VUwzoZPU1.jpg",
    actors: ["Samantha Locke", "Derek Yule"],
    reviews: [
      {
        reviewer: "Sarah Connell",
        text: "Thrilling and full of suspense, a true science fiction masterpiece.",
      },
      {
        reviewer: "Gary Mason",
        text: "Fascinating concept but lacking in character development.",
      },
    ],
    keywords: ["science fiction", "thriller", "experiment"],
  },
  {
    id: "4",
    title: "Ocean's Echo",
    description:
      "A deep sea diver uncovers a secret underwater city with a dark past.",
    poster:
      "https://atthemovies.uk/cdn/shop/products/Gladiator2000us27x40in195u.jpg?v=1621385091",
    actors: ["Marie Curran", "Timothy Grant"],
    reviews: [
      {
        reviewer: "Olivia Rue",
        text: "A mesmerizing underwater adventure that explores the unknown.",
      },
      {
        reviewer: "Ethan Stoll",
        text: "Stunning visuals but the story leaves something to be desired.",
      },
    ],
    keywords: ["adventure", "mystery", "underwater"],
  },
  {
    id: "5",
    title: "Skies of Rage",
    description:
      "Fighter pilots engage in a fierce battle to control the skies in a war-torn world.",
    poster:
      "https://atthemovies.uk/cdn/shop/products/Gladiator2000us27x40in195u.jpg?v=1621385091",
    actors: ["Victor Lane", "Jessica Morris"],
    reviews: [
      {
        reviewer: "Michael Torres",
        text: "High-octane aerial action that keeps you at the edge of your seat.",
      },
      {
        reviewer: "Laura Gibbs",
        text: "More action than story, but still a thrilling ride.",
      },
    ],
    keywords: ["action", "war", "aerial combat"],
  },
  {
    id: "6",
    title: "Ghosts of the Past",
    description:
      "A historian uncovers secrets in an ancient manuscript that point to a hidden treasure.",
    poster:
      "https://image.tmdb.org/t/p/original/xlaY2zyzMfkhk0HSC5VUwzoZPU1.jpg",
    actors: ["Helen Gear", "Mark Ruffalo"],
    reviews: [
      {
        reviewer: "Cindy Loo",
        text: "A thrilling mystery that combines history with adventure.",
      },
      {
        reviewer: "Roger Ebert",
        text: "Captivating from start to finish with a smart blend of history and drama.",
      },
    ],
    keywords: ["history", "treasure", "adventure", "mystery"],
  },
  {
    id: "7",
    title: "Starship Journey",
    description:
      "A crew of astronauts embarks on a mission to explore a newly discovered exoplanet.",
    poster:
      "https://atthemovies.uk/cdn/shop/products/Gladiator2000us27x40in195u.jpg?v=1621385091",
    actors: ["Chris Pine", "Zoe Saldana"],
    reviews: [
      {
        reviewer: "Amy Pond",
        text: "A gripping space opera with deep, complex characters.",
      },
      {
        reviewer: "Jake Blues",
        text: "Visually stunning, but could use more depth in the plot.",
      },
    ],
    keywords: ["space", "exploration", "sci-fi", "astronauts"],
  },
  {
    id: "8",
    title: "Revenge of the Fallen",
    description:
      "An exiled prince returns to reclaim his throne from his treacherous uncle.",
    poster:
      "https://cdn.europosters.eu/image/1300/art-photo/the-dark-knight-trilogy-a-fire-will-rise-i184446.jpg",

    actors: ["Liam Neeson", "Toby Maguire"],
    reviews: [
      {
        reviewer: "Diana Prince",
        text: "A spectacular tale of betrayal and revenge.",
      },
      {
        reviewer: "Clark Kent",
        text: "Epic in scale, with a compelling storyline.",
      },
    ],
    keywords: ["revenge", "epic", "throne", "betrayal"],
  },
  {
    id: "9",
    title: "Silent Whispers",
    description:
      "A young woman with the ability to hear the thoughts of others tries to solve a murder.",
    poster:
      "https://cdn.europosters.eu/image/1300/art-photo/the-dark-knight-trilogy-a-fire-will-rise-i184446.jpg",

    actors: ["Emma Watson", "Daniel Radcliffe"],
    reviews: [
      {
        reviewer: "Bruce Wayne",
        text: "An intriguing supernatural thriller with a strong lead performance.",
      },
      {
        reviewer: "Tony Stark",
        text: "Engaging and eerie, with plenty of twists.",
      },
    ],
    keywords: ["supernatural", "murder", "mystery", "thriller"],
  },
  {
    id: "10",
    title: "Lights in the Abyss",
    description:
      "Marine biologists discover an underwater alien species in the depths of the Pacific Ocean.",
    poster:
      "https://cdn.europosters.eu/image/1300/art-photo/the-dark-knight-trilogy-a-fire-will-rise-i184446.jpg",
    actors: ["Scarlett Johansson", "Jeremy Renner"],
    reviews: [
      {
        reviewer: "Natasha Romanoff",
        text: "A unique blend of science fiction and deep-sea adventure.",
      },
      {
        reviewer: "Nick Fury",
        text: "Compelling and thought-provoking, a must-watch for sci-fi fans.",
      },
    ],
    keywords: ["alien", "ocean", "adventure", "sci-fi"],
  },
  {
    id: "11",
    title: "Desert Winds",
    description:
      "Two estranged brothers face their past in the unforgiving landscape of the Mojave Desert.",
    poster:
      "https://marketplace.canva.com/EAE-LsW7NOM/1/0/1131w/canva-dark-grey-white-simple-mystery-forest-movie-promotion-poster-1_xk_PDVROU.jpg",
    actors: ["Ethan Hawke", "River Phoenix"],
    reviews: [
      {
        reviewer: "Sarah Conner",
        text: "A raw, emotional drama that brilliantly portrays brotherhood and redemption.",
      },
      {
        reviewer: "John Rambo",
        text: "Heart-wrenching and beautifully shot, a true artistic triumph.",
      },
    ],
    keywords: ["drama", "brotherhood", "redemption", "desert"],
  },
  {
    id: "12",
    title: "Urban Myths",
    description:
      "A skeptical journalist debunks urban legends, only to find one that proves dangerously true.",
    poster:
      "https://marketplace.canva.com/EAE-LsW7NOM/1/0/1131w/canva-dark-grey-white-simple-mystery-forest-movie-promotion-poster-1_xk_PDVROU.jpg",
    actors: ["Bruce Willis", "Samuel L. Jackson"],
    reviews: [
      {
        reviewer: "Dana Scully",
        text: "Chillingly plausible and thrilling from start to finish.",
      },
      {
        reviewer: "Fox Mulder",
        text: "A captivating blend of suspense and horror that keeps you guessing.",
      },
    ],
    keywords: ["thriller", "mystery", "horror", "urban"],
  },
  {
    id: "13",
    title: "Highland Games",
    description:
      "An American joins a historic Scottish clan gathering and discovers his roots and rivals.",
    poster:
      "https://marketplace.canva.com/EAE-LsW7NOM/1/0/1131w/canva-dark-grey-white-simple-mystery-forest-movie-promotion-poster-1_xk_PDVROU.jpg",
    actors: ["Chris Hemsworth", "Tom Hiddleston"],
    reviews: [
      {
        reviewer: "Merida DunBroch",
        text: "An exhilarating, funny, and culturally rich film.",
      },
      {
        reviewer: "Connor MacLeod",
        text: "Full of heart, humor, and Scottish spirit!",
      },
    ],
    keywords: ["culture", "adventure", "comedy", "Scotland"],
  },
  {
    id: "14",
    title: "Timekeeper's Fate",
    description:
      "A clockmaker discovers a timepiece that can alter the past, with unexpected consequences.",
    poster:
      "https://rukminim2.flixcart.com/image/850/1000/jf8khow0/poster/a/u/h/small-hollywood-movie-poster-blade-runner-2049-ridley-scott-original-imaf3qvx88xenydd.jpeg?q=20&crop=false",
    actors: ["Cillian Murphy", "Ben Whishaw"],
    reviews: [
      {
        reviewer: "Hermione Granger",
        text: "A thought-provoking narrative on time and its perils.",
      },
      {
        reviewer: "Doctor Who",
        text: "A clever, twisting sci-fi adventure that questions reality.",
      },
    ],
    keywords: ["sci-fi", "drama", "time travel", "mystery"],
  },
  {
    id: "15",
    title: "Pirates of the Sky",
    description:
      "Sky pirates raid luxury airships, but one pirate’s heart is swayed by a young heiress.",
    poster:
      "https://marketplace.canva.com/EAE-LsW7NOM/1/0/1131w/canva-dark-grey-white-simple-mystery-forest-movie-promotion-poster-1_xk_PDVROU.jpg",
    actors: ["Johnny Depp", "Keira Knightley"],
    reviews: [
      {
        reviewer: "Jack Sparrow",
        text: "An action-packed, thrilling ride above the clouds!",
      },
      {
        reviewer: "Elizabeth Swann",
        text: "Romantic and adventurous with stunning aerial battles.",
      },
    ],
    keywords: ["adventure", "romance", "action", "pirates"],
  },
  {
    id: "16",
    title: "Echoes of Silence",
    description:
      "In a post-apocalyptic world, a lone survivor with the ability to communicate with the dead seeks the last remaining human settlement.",
    poster:
      "https://rukminim2.flixcart.com/image/850/1000/jf8khow0/poster/a/u/h/small-hollywood-movie-poster-blade-runner-2049-ridley-scott-original-imaf3qvx88xenydd.jpeg?q=20&crop=false",
    actors: ["Cate Blanchett", "Michael Fassbender"],
    reviews: [
      {
        reviewer: "Lara Croft",
        text: "A haunting exploration of solitude and survival.",
      },
      {
        reviewer: "Indiana Jones",
        text: "Eerily beautiful and profoundly moving.",
      },
    ],
    keywords: ["post-apocalyptic", "supernatural", "survival", "thriller"],
  },
  {
    id: "17",
    title: "Neon Dreams",
    description:
      "An aspiring artist descends into the neon-lit underworld of a futuristic metropolis.",
    poster:
      "https://rukminim2.flixcart.com/image/850/1000/jf8khow0/poster/a/u/h/small-hollywood-movie-poster-blade-runner-2049-ridley-scott-original-imaf3qvx88xenydd.jpeg?q=20&crop=false",
    actors: ["Ryan Gosling", "Emma Stone"],
    reviews: [
      {
        reviewer: "Rick Deckard",
        text: "A visually stunning journey through a cyberpunk reality.",
      },
      {
        reviewer: "Sarah Connor",
        text: "Intense and provocative with a memorable visual style.",
      },
    ],
    keywords: ["cyberpunk", "futuristic", "drama", "adventure"],
  },
  {
    id: "18",
    title: "Guardians of the Forest",
    description:
      "A group of children discover they are descendants of forest spirits and must save their land from ecological disaster.",
    poster:
      "https://marketplace.canva.com/EAE-LsW7NOM/1/0/1131w/canva-dark-grey-white-simple-mystery-forest-movie-promotion-poster-1_xk_PDVROU.jpg",
    actors: ["Dakota Fanning", "Elle Fanning"],
    reviews: [
      {
        reviewer: "Yoda",
        text: "A compelling tale of magic and responsibility.",
      },
      {
        reviewer: "Gandalf",
        text: "Heartwarming and magical, with a powerful message.",
      },
    ],
    keywords: ["fantasy", "adventure", "children", "ecology"],
  },
  {
    id: "19",
    title: "The Last Light",
    description:
      "A lighthouse keeper on a remote island uncovers a secret that changes his perception of the world.",
    poster:
      "https://i.pinimg.com/originals/2a/c4/1d/2ac41deddbcf11c4572417105e58f7ae.gif",
    actors: ["Tom Hanks", "Helen Mirren"],
    reviews: [
      {
        reviewer: "Amelia Earhart",
        text: "A mesmerizing and introspective narrative.",
      },
      {
        reviewer: "Christopher Columbus",
        text: "A story that shines a light on the human spirit.",
      },
    ],
    keywords: ["drama", "mystery", "isolation", "lighthouse"],
  },
  {
    id: "20",
    title: "Whispers of War",
    description:
      "In the midst of a silent war, a young soldier learns that the real enemy is not always in sight.",
    poster:
      "https://i.pinimg.com/originals/2a/c4/1d/2ac41deddbcf11c4572417105e58f7ae.gif",
    actors: ["Chris Evans", "Scarlett Johansson"],
    reviews: [
      {
        reviewer: "Napoleon Bonaparte",
        text: "A gripping war drama that explores the psychological battles faced by soldiers.",
      },
      {
        reviewer: "Julius Caesar",
        text: "Powerful and thought-provoking, a true masterpiece.",
      },
    ],
    keywords: ["war", "drama", "psychological", "military"],
  },
  {
    id: "21",
    title: "Mystic Chords of Memory",
    description:
      "A renowned pianist returns to her hometown and unravels her family's complex history through music.",
    poster:
      "https://i.pinimg.com/originals/2a/c4/1d/2ac41deddbcf11c4572417105e58f7ae.gif",
    actors: ["Natalie Portman", "Jude Law"],
    reviews: [
      {
        reviewer: "Beethoven",
        text: "A symphony of emotions, brilliantly executed.",
      },
      {
        reviewer: "Mozart",
        text: "Elegant and profound, a musical journey through time.",
      },
    ],
    keywords: ["drama", "music", "family", "mystery"],
  },
  {
    id: "22",
    title: "Shadows in the Rain",
    description:
      "A detective hunts a serial killer who uses heavy rainfalls to cover his deadly tracks.",
    poster: "https://i.ebayimg.com/images/g/dpcAAOSw5fZgreGs/s-l1200.webp",
    actors: ["Keanu Reeves", "Mark Wahlberg"],
    reviews: [
      {
        reviewer: "Sherlock Holmes",
        text: "A dark and stormy thriller with brilliant detective work.",
      },
      {
        reviewer: "Jane Marple",
        text: "Intricately plotted and atmospherically rich.",
      },
    ],
    keywords: ["thriller", "mystery", "detective", "rain"],
  },
  {
    id: "23",
    title: "The Winter Crown",
    description:
      "In a kingdom besieged by eternal winter, a young princess discovers an ancient prophecy that could restore warmth and light, but first, she must navigate the dangerous politics of her own court.",
    poster:
      "https://m.media-amazon.com/images/M/MV5BMTk2MjcxNjMzN15BMl5BanBnXkFtZTgwMTE3OTEwNjE@._V1_.jpg",
    actors: ["Emma Stone", "Saoirse Ronan"],
    reviews: [
      {
        reviewer: "Aragorn",
        text: "A majestic and epic fantasy that captures the imagination and the struggles of a heroine destined for greatness.",
      },
      {
        reviewer: "Galadriel",
        text: "Richly woven with magic and the harsh realities of royal duty.",
      },
    ],
    keywords: ["fantasy", "epic", "magic", "princess"],
  },
  {
    id: "24",
    title: "Beneath the Surface",
    description:
      "After a mysterious seismic event, a team of geologists ventures deep into the Earth's crust, only to find themselves in an undiscovered world complete with its own civilization.",
    poster:
      "https://m.media-amazon.com/images/M/MV5BMTk2MjcxNjMzN15BMl5BanBnXkFtZTgwMTE3OTEwNjE@._V1_.jpg",
    actors: ["Daniel Craig", "Rosamund Pike"],
    reviews: [
      {
        reviewer: "Indiana Jones",
        text: "An underground adventure that turns into a fight for survival against all odds.",
      },
      {
        reviewer: "Lara Croft",
        text: "Thrilling and packed with unexpected turns, this journey to the center of the Earth redefines adventure.",
      },
    ],
    keywords: ["adventure", "discovery", "geology", "mystery"],
  },
  {
    id: "25",
    title: "Lost Melodies",
    description:
      "A world-famous composer loses his ability to hear music and must find a new connection to his muse through the vibrant world of visual arts, exploring an unlikely friendship with a misunderstood painter.",
    poster:
      "https://m.media-amazon.com/images/M/MV5BMTk2MjcxNjMzN15BMl5BanBnXkFtZTgwMTE3OTEwNjE@._V1_.jpg",
    actors: [
      "Jeff Bridges",
      "Meryl Streep",
      "Leonardo Dicaprio",
      "Brad Pitt",
      "Megane Fox",
      "Tom Hardy",
      "Jessica Chastain",
    ],
    reviews: [
      {
        reviewer: "Wolfgang Amadeus Mozart",
        text: "A profound narrative that explores the depths of artistic inspiration and the bonds that music and art can create.",
      },
      {
        reviewer: "Frida Kahlo",
        text: "Visually stunning and emotionally resonant, a true masterpiece of personal and artistic transformation.",
      },
    ],
    keywords: ["drama", "music", "art", "inspiration"],
  },
  {
    id: "26",
    title: "Chase the Storm",
    description:
      "An expert storm chaser and a rookie meteorologist partner up for the pursuit of recording the most extreme weather phenomena, only to discover a pattern that could explain recent catastrophic events.",
    poster:
      "https://m.media-amazon.com/images/M/MV5BMTk2MjcxNjMzN15BMl5BanBnXkFtZTgwMTE3OTEwNjE@._V1_.jpg",
    actors: ["Tom Hardy", "Jessica Chastain"],
    reviews: [
      {
        reviewer: "Stormy Daniels",
        text: "An electrifying journey into the heart of nature's fury, packed with suspense and groundbreaking visual effects.",
      },
      {
        reviewer: "Al Roker",
        text: "A whirlwind of drama and excitement that keeps you on the edge of your seat.",
      },
    ],
    keywords: ["action", "adventure", "weather", "thriller"],
  },
  {
    id: "27",
    title: "Echoes of the Past",
    description:
      "A determined archaeologist unearths a forgotten tomb in Egypt, setting off a chain of events that reveal secrets about the ancient world and her own family's mysterious past.",
    poster: "https://i.ebayimg.com/images/g/dpcAAOSw5fZgreGs/s-l1200.webp",
    actors: ["Nicole Kidman", "Ralph Fiennes"],
    reviews: [
      {
        reviewer: "Howard Carter",
        text: "A captivating blend of history and suspense, this archaeological thriller digs deep into the heart of ancient mysteries.",
      },
      {
        reviewer: "Cleopatra",
        text: "Spellbinding and richly detailed, a journey through time that mesmerizes and educates.",
      },
    ],
    keywords: ["history", "archaeology", "mystery", "thriller"],
  },
  {
    id: "28",
    title: "Night Sky",
    description:
      "When a mysterious phenomenon causes the stars to disappear from the night sky, a reclusive astronomer and a charismatic public speaker embark on a cross-country journey to restore the public's hope and uncover the truth behind the celestial blackout.",
    poster: "https://i.ebayimg.com/images/g/dpcAAOSw5fZgreGs/s-l1200.webp",
    actors: ["George Clooney", "Julia Roberts"],
    reviews: [
      {
        reviewer: "Carl Sagan",
        text: "An inspiring tale that combines a poignant human story with the wonders of the cosmos.",
      },
      {
        reviewer: "Neil deGrasse Tyson",
        text: "Engaging and thought-provoking, a stellar exploration of humanity's place in the universe.",
      },
    ],
    keywords: ["sci-fi", "drama", "adventure", "astronomy"],
  },
  {
    id: "29",
    title: "Sands of Time",
    description:
      "In the sprawling dunes of the Sahara, a time capsule is discovered that predicts historical events with uncanny accuracy, leading a group of scholars to confront their preconceptions about fate and free will.",
    poster:
      "https://marketplace.canva.com/EAE-LsW7NOM/1/0/1131w/canva-dark-grey-white-simple-mystery-forest-movie-promotion-poster-1_xk_PDVROU.jpg",
    actors: ["Hugh Jackman", "Penelope Cruz"],
    reviews: [
      {
        reviewer: "T.E. Lawrence",
        text: "A thrilling adventure that challenges both the mind and the spirit.",
      },
      {
        reviewer: "Richard Francis Burton",
        text: "A profound look at the sands of time and the threads that weave the tapestry of history.",
      },
    ],
    keywords: ["adventure", "mystery", "history", "time travel"],
  },
  {
    id: "30",
    title: "Final Note",
    description:
      "A once-celebrated pianist has one final concert to prove herself amidst personal trials and the fading twilight of her career, revisiting her past and rediscovering her passion for music.",
    poster:
      "https://marketplace.canva.com/EAE-LsW7NOM/1/0/1131w/canva-dark-grey-white-simple-mystery-forest-movie-promotion-poster-1_xk_PDVROU.jpg",
    actors: ["Kate Winslet", "Jude Law"],
    reviews: [
      {
        reviewer: "Ludwig van Beethoven",
        text: "An evocative performance that strikes all the right chords, blending heartache with triumph.",
      },
      {
        reviewer: "Frederic Chopin",
        text: "A resonant and beautifully crafted tale of redemption and the enduring power of art.",
      },
    ],
    keywords: ["drama", "music", "comeback", "inspirational"],
  },
];
