export interface APIConfig {
  baseURL: string;
}
export type SetterFuncType<T> = (data: T) => void;
export type ErrorHandlerFuncType = (error: Error) => void;
