import { ErrorHandlerFuncType, SetterFuncType } from "./types";
import { mockFetcher } from "../mocks/mock-API";
import { mockMovies } from "../mocks/mock-data";
import { Movie, PartialMovie } from "../types/movie-types";

//Commented parts of this file are for real API, but now we are calling to my mock API
class MovieAPI {
  // private readonly baseURL;
  //
  // constructor(config: APIConfig) {
  //   this.baseURL = config.baseURL;
  // }

  fetchRandomMovies = async (
    setterFunc?: SetterFuncType<Array<PartialMovie>>,
    errorHandler?: ErrorHandlerFuncType,
  ) => {
    // const data = await fetch(`${this.baseURL}/random?count=10`);

    const randomSelectedMovies = mockMovies
      .sort(() => 0.5 - Math.random())
      .slice(0, 10)
      .map(({ title, poster, id }) => ({ title, poster, id }));
    try {
      const data: Array<PartialMovie> = await mockFetcher(randomSelectedMovies);

      if (setterFunc) {
        setterFunc(data);
      } else {
        return data;
      }
    } catch (e) {
      if (errorHandler) {
        errorHandler(e);
      } else {
        throw new Error(e.message);
      }
    }
  };

  searchMovies = async (
    query: string,
    setterFunc?: SetterFuncType<Array<PartialMovie>>,
    errorHandler?: ErrorHandlerFuncType,
  ) => {
    // const data = await fetch(`${this.baseURL}/search?title=${query}`);

    const filteredMovies = mockMovies
      .filter(
        (movie) =>
          movie.title.toLowerCase().includes(query.toLowerCase()) ||
          movie.keywords.join(",").includes(query.toLowerCase()),
      )
      .map(({ title, poster, id }) => ({
        title,
        poster,
        id,
      }));

    try {
      const data: Array<PartialMovie> = await mockFetcher(filteredMovies);

      if (setterFunc) {
        setterFunc(data);
      } else {
        return data;
      }
    } catch (e) {
      if (errorHandler) {
        errorHandler(e);
      } else {
        throw new Error(e.message);
      }
    }
  };

  fetchMovieDetails = async (
    id: string,
    setterFunc?: SetterFuncType<Movie>,
    errorHandler?: ErrorHandlerFuncType,
  ) => {
    // const data = await fetch(`${this.baseURL}/movie/${id}`);

    const selectedMovie = mockMovies.find((movie) => movie.id === id);
    try {
      const data: Movie = await mockFetcher(selectedMovie);

      if (setterFunc) {
        setterFunc(data);
      } else {
        return data;
      }
    } catch (e) {
      if (errorHandler) {
        errorHandler(e);
      } else {
        throw new Error(e.message);
      }
    }
  };
}

export default MovieAPI;
