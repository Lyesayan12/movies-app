import useHome from "./useHome";
import MovieGrid from "../../components/movie-grid/MovieGrid";

const Home = () => {
  const { movieList, loading } = useHome();
  return <MovieGrid loading={loading} movieList={movieList} />;
};

export default Home;
