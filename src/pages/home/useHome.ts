import { useEffect, useState } from "react";
import movieFetcher from "../../api/api";
import { useDispatch, useSelector } from "react-redux";
import { selectHomepageMovies } from "../../store/movies-slice/selector";
import { PartialMovie } from "../../types/movie-types";
import { movieActions } from "../../store/movies-slice/movies";
import { useToaster } from "../../contexts/toaster/ToasterContext";

const useHome = () => {
  const [loading, setLoading] = useState(false);
  const movieList = useSelector(selectHomepageMovies);
  const dispatch = useDispatch();

  const { addToast } = useToaster();

  useEffect(() => {
    const setMoviesList = (data: Array<PartialMovie>) => {
      dispatch(movieActions.setHomepageMovies(data));
    };
    const errorHandler = (error: Error) => {
      addToast(error.message, 3000);
    };
    setLoading(true);
    movieFetcher
      .fetchRandomMovies(setMoviesList, errorHandler)
      .finally(() => setLoading(false));
  }, []);

  return { movieList, loading };
};

export default useHome;
