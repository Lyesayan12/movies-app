import { useParams } from "react-router-dom";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { selectSingleMovie } from "../../store/movies-slice/selector";
import { Movie } from "../../types/movie-types";
import { movieActions } from "../../store/movies-slice/movies";
import movieFetcher from "../../api/api";
import { useToaster } from "../../contexts/toaster/ToasterContext";

const useMovieDetails = () => {
  const { id } = useParams();
  const [loading, setLoading] = useState(false);
  const movieDetails = useSelector(selectSingleMovie);
  const dispatch = useDispatch();

  const { addToast } = useToaster();

  useEffect(() => {
    const setSingleMovie = (data: Movie) => {
      dispatch(movieActions.selectSingleMovie(data));
    };
    const errorHandler = (error: Error) => {
      addToast(error.message, 3000);
    };
    setLoading(true);
    if (id) {
      movieFetcher
        .fetchMovieDetails(id, setSingleMovie, errorHandler)
        .finally(() => setLoading(false));
    }
  }, [id]);

  return { movieDetails, loading };
};
export default useMovieDetails;
