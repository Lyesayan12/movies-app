import styles from "./styles.module.css";
import DetailsPageSkeleton from "../../components/details-page-skeleton/DetailsPageSkeleton";
import useMovieDetails from "./useMovieDetails"; // Ensure this is correctly imported

const MovieDetail = () => {
  const { movieDetails: movie, loading } = useMovieDetails();
  if (loading) {
    return <DetailsPageSkeleton />;
  }

  return (
    <div className={styles.container}>
      <h1>{movie.title}</h1>
      <div className={styles.content}>
        <img src={movie.poster} alt={movie.title} className={styles.poster} />
        <div className={styles.details}>
          <p>{movie.description}</p>
          <h3>Actors</h3>
          <div className={styles.actors}>
            {movie.actors?.map((actor) => (
              <div key={actor} className={styles.actorItem}>
                {actor}
              </div>
            ))}
          </div>
          <h3>Reviews</h3>
          <div className={styles.reviews}>
            {movie.reviews?.map((review) => (
              <div key={review.reviewer} className={styles.review}>
                <strong>{review.reviewer}</strong>
                <p>{review.text}</p>
              </div>
            ))}
          </div>
          <h3>Keywords</h3>
          <ul className={styles.keywords}>
            {movie.keywords?.map((keyword) => <li key={keyword}>{keyword}</li>)}
          </ul>
        </div>
      </div>
    </div>
  );
};

export default MovieDetail;
