import useSearchResults from "./useSearchResults";
import MovieGrid from "../../components/movie-grid/MovieGrid";
import styles from "./styles.module.css";

const SearchResults = () => {
  const { movies, loading, query } = useSearchResults();

  if (movies.length === 0 && !loading) {
    return <div className={styles.noResults}>No results found.</div>;
  }

  return (
    <div className={styles.searchResultsContainer}>
      {!loading && (
        <h2 className={styles.fixedTitle}>
          Showing results matching your search &quot;{query}&quot;
        </h2>
      )}
      <MovieGrid loading={loading} movieList={movies} />
    </div>
  );
};

export default SearchResults;
