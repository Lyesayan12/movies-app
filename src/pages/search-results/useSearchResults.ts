import { useEffect, useState } from "react";
import movieFetcher from "../../api/api";
import { useDispatch, useSelector } from "react-redux";
import { selectSearchResults } from "../../store/movies-slice/selector";
import { PartialMovie } from "../../types/movie-types";
import { movieActions } from "../../store/movies-slice/movies";
import { useSearchParams } from "react-router-dom";
import { useToaster } from "../../contexts/toaster/ToasterContext";

const useHome = () => {
  const [searchParams] = useSearchParams();
  const [loading, setLoading] = useState(false);
  const searchResults = useSelector(selectSearchResults);
  const dispatch = useDispatch();
  const query = searchParams.get("query");

  const { addToast } = useToaster();

  useEffect(() => {
    const setSearchResults = (data: Array<PartialMovie>) => {
      dispatch(movieActions.setSearchResults(data));
    };
    const errorHandler = (error: Error) => {
      addToast(error.message, 3000);
    };
    if (query) {
      setLoading(true);
      movieFetcher
        .searchMovies(query, setSearchResults, errorHandler)
        .finally(() => setLoading(false));
    }
  }, [query]);

  return { movies: searchResults, loading, query };
};

export default useHome;
