import styles from "./style.module.css";

const NotFound = () => {
  return (
    <div className={styles.container}>
      <h1>404 Not Found</h1>
      <p>The page you are looking for does not exist.</p>
      <a href="/">Go to homepage</a>
    </div>
  );
};

export default NotFound;
