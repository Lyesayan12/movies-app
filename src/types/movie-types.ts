export type Movie = {
  title: string;
  description: string;
  poster: string;
  actors: Array<string>;
  reviews: Array<{ reviewer: string; text: string }>;
  keywords: Array<string>;
  id: string;
};

export type PartialMovie = Pick<Movie, "title" | "id" | "poster">;
